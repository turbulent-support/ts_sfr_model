# Turbulent Support SFR model

This repository contains the code needed to use the TS analytical model for the SFR based on the gravo-turbulent theory.
The TS model is a substantial refinement of the [Henebelle & Chabrier (2011)](https://ui.adsabs.harvard.edu/abs/2011ApJ...743L..29H/abstract) model and is described in paper I [(Henebelle, Brucy & Colman, 2024)](https://ui.adsabs.harvard.edu/abs/2024A%26A...690A..43H/abstract).
It has been tested against a set of numerical simulations in paper II [(Brucy, Henebelle, Colman et al., 2024)](https://ui.adsabs.harvard.edu/abs/2024A%26A...690A..44B/abstract).

This repository also contains notebooks and data to reproduce the plots of both papers.

## Usage

### ts_sfr.py and ts_lib.py

The `ts_sfr.py` file is the main file of the repository. It contains the function `sfr_ts()` that computes the SFR according to the TS theory.

Please read the docstrings for the usage and results.

### generate_model_bank.py

The `generate_model_bank.py` file is used to generate a model bank based on the TS theory. It provides a way to call sfr_ts on a set of parameters, which is useful to plot the results of the model.

### compare_sims.py

The `compare_sims.py` file contains helper functions used to compare the results of the simulations from paper II and the model.

