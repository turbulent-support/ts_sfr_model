"""
Data reference: Brucy et al. (2024)
"""

import numpy as np
import pandas as pd
import ts_sfr as ts
from ts_lib import fit_pdf_CH_constrained


## SFR and theory


def normalisation_SFR(L, n0=1.5):
    """
    normalisation of the SFR
    """
    #  multiply by the box length^3 by the density and divide by np.sqrt(3.*np.pi/32.) / sqrt(rho_O G)  (FF)(
    norm = (
        ((L * 3.08e18) ** 3)
        * n0
        * 1.6e-24
        * np.sqrt(6.67e-8 * n0 * 1.6e-24)
        / np.sqrt(3.0 * np.pi / 32.0)
    )
    # divide by Ms and multiply by yr
    norm = norm / (2.0e33) * (3600.0 * 24.0 * 365.0)
    return norm


def compute_SFR_theory_run(
    turbox,
    run,
    max_T=1.0,
    Mach_support_factor=1.0,
    min_cells=1,
    **kwargs,
):
    L = turbox.L[run]
    lvl = turbox.res[run]
    x_pdf = turbox.pdf_logrho_x[run]
    y_pdf = turbox.pdf_logrho_y[run]
    S_logrho_V = turbox.S_logrho_V[run]
    Mach = turbox.Mach[run]
    cs = turbox.sigma[run] * 1e5 / Mach  # cm.s-1
    comp = turbox.comp[run]
    n0 = turbox.n0[run]
    b = 1 - (2 / 3.0) * (1 - comp)
    T_CH, perr = fit_pdf_CH_constrained(x_pdf, y_pdf, S_logrho_V, max_T)
    eta_d = turbox.eta[run]
    results_CH = ts.sfr_ts(
        b=b,
        Lreso=L * 2.0 ** (-lvl) * min_cells,
        Lc=L / 3,
        cs=cs,
        eta_d=eta_d,
        n0=n0,
        pdf_kind="castainghopkins",
        Mach=Mach,
        T_CH=T_CH,
        S_lnrho=S_logrho_V,
        Mach_support_factor=Mach_support_factor,
        **kwargs,
    )
    results_LN = ts.sfr_ts(
        b=b,
        Lreso=L * 2.0 ** (-lvl) * min_cells,
        Lc=L / 3,
        cs=cs,
        eta_d=eta_d,
        n0=n0,
        pdf_kind="lognormal",
        Mach=Mach,
        T_CH=T_CH,
        S_lnrho=S_logrho_V,
        **kwargs,
    )
    return T_CH, results_CH, results_LN


def compute_SFR_theory(turbox, **kwargs):
    turbox["T_Hopkins"] = np.zeros_like(turbox["S_logrho_V"])
    turbox["sfrHB_LN"] = np.zeros_like(turbox["S_logrho_V"])
    turbox["sfrHB"] = np.zeros_like(turbox["S_logrho_V"])
    turbox["sfrFF"] = np.zeros_like(turbox["S_logrho_V"])
    turbox["sfrPN"] = np.zeros_like(turbox["S_logrho_V"])
    turbox["sfrPNMFF"] = np.zeros_like(turbox["S_logrho_V"])

    results_CH_d = {}
    results_LN_d = {}
    for run in turbox.index:
        T_Hopkins, results_CH, results_LN = compute_SFR_theory_run(
            turbox, run, **kwargs
        )
        turbox.loc[run, "T_Hopkins"] = T_Hopkins
        turbox.loc[run, "sfrHB_LN"] = results_LN["sfr"]
        turbox.loc[run, "sfrHB"] = results_CH["sfr"]
        turbox.loc[run, "sfrPN"] = results_LN["sfr_PN"]
        results_CH_d[run] = results_CH
        results_LN_d[run] = results_LN
    return results_CH_d, results_LN_d


def normalize(turbox):
    turbox.sfr = turbox.sfr / normalisation_SFR(turbox.L)
    turbox.sfr_err = turbox.sfr_err / normalisation_SFR(turbox.L)
    turbox.sigma = turbox.sigma / np.sqrt(2)
    turbox.Mach = turbox.Mach / np.sqrt(2)
