"""
Computation of the SFR estimate in a gravoturbulent medium,
according to Hennebelle, Brucy & Colman 2024.
"""

import numpy as np
import scipy as sc

from ts_lib import pdf_CH, compute_mass_spectrum


#######################################################################################
#######################################################################################
def sfr_padoan(
    mach,
    eps,
    b,
    pdf_kind="lognormal",
    T_CH=0.1,
    S_lnrho=None,
    Nbin=1000,
    ln_rho=None,
    pdf=None,
):
    """Compute SFR with Padoan & Nordlund formula

    Parameters
    ----------
    mach : float
        Mach number
    eps : float
        factor for critical density
    b : float
        b parameter for the lognormal pdf
    pdf_kind : str, optional
        "lognormal", "castainghopkins" or "custom", by default "lognormal"
    T_CH : float, optional
        T parameter for castainghopkins pdf, by default 0.1
    S_lnrho : float, optional
        width of the castainghopkins pdf, by default None
    Nbin : int, optional
        Number of bins for the castainghopkins pdf, by default 1000
    ln_rho : np.ndarray, optional
        custom pdf (bins), by default None
    pdf :  np.ndarray, optional
        custom pdf (value), by default None

    Returns
    -------
    sfr
        single freefall sfr
    sfr_mff
        multi freefall sfr
    """
    sigma = np.sqrt(np.log(1.0 + b**2 * (mach) ** 2))

    xcrit = eps * mach**4

    if pdf_kind == "lognormal":
        sfr = (
            np.sqrt(xcrit)
            / 2.0
            * (
                1.0
                + sc.special.erf(
                    (sigma**2 - 2.0 * np.log(xcrit)) / (2.0**1.5 * sigma)
                )
            )
        )

        sfr_mff = (
            0.5
            * np.exp(3.0 / 8.0 * sigma**2)
            * (
                1.0
                + sc.special.erf((sigma**2 - np.log(xcrit)) / (2.0**0.5 * sigma))
            )
        )

    elif pdf_kind == "castainghopkins":
        if S_lnrho is None:
            S_lnrho = sigma

        ln_rho = np.linspace(-15, 25, Nbin)
        pdf = pdf_CH(ln_rho, S_lnrho, T_CH)

    if pdf_kind == "castainghopkins" or pdf_kind == "custom":
        mask = ln_rho > np.log(xcrit)

        d_ln_rho = np.roll(ln_rho, -1) - ln_rho
        d_ln_rho[-2] = d_ln_rho[-1]

        sfr = np.sum(pdf[mask] * np.exp(ln_rho[mask]) * d_ln_rho[mask]) * np.sqrt(xcrit)

        sfr_mff = np.sum(pdf[mask] * (np.exp(ln_rho[mask])) ** 1.5 * d_ln_rho[mask])

    return sfr, sfr_mff


#######################################################################################
#######################################################################################
def sfr_ts(
    Li=500.0,
    n0=1.0,
    Mach=5,
    cs=3.0e4,
    Lreso=1e-4,
    eta_v=0.4,
    eta_d=0.2,
    pdf_kind="lognormal",
    b=0.5,
    S_lnrho=None,
    T_CH=0.1,
    ycut=1,
    scale_T_with_R=True,
    only_PN=False,
    **kwargs,
):
    """Compute the SFR in a turbulent medium using the Turbulent suport model

    Parameters
    ----------
    Li : float, optional
        injection scale, by default 500.0
    n0 : float, optional
        mean number density, by default 1.0
    Mach : float, optional
        Mean mach number at injection scale, by default 5
    cs : float, optional
        Sound speed, by default 3.0e4
    Lreso : float, optional
        Resolution scale, by default 1e-4
    eta_v : float, optional
         indice of the velocity power spectrum of, by default 0.4
    eta_d : float, optional
        indice of power spectrum of log rho, by default 0.2
    pdf_kind : str, optional
        PDF can be 'lognormal' or 'castainghopkins' (from Castaing 1996, Hopkins 2013), by default "lognormal"
    b : float
        width of the lognormal is ln(1+b^2*Mach^2). Linked to the compressiblility of the driving.
    S_lnrho : float
        the variance of log rho (volumic) - noted S_\delta in Hennebelle, Brucy & Colman 2024
    T_CH : float
        T parameter in Hopkings 2013
    ycut : float, optional
        largest self-gravitating fluctuations in fraction of the injection scale Li, by default 1
    scale_T_with_R : bool, optional
        scale T_CH with R the same way the variance S is scaled, by default True
    only_PN : bool, optional
        Only Padoan & Nordlund sfr, by default False

    Returns
    -------
    dict
        Dictionnarry containing the results of the model:
        - "sfr": float, SFR according to the TS model
        - "sfr_with_ff_time": float, SFR according to the TS model with freefall time instead of the maximum between replenishment time and freefall time
        - "jeans_length": float, Jeans length
        - "sfr_PN": float, SFR according to the Padoan & Nordlund model
        - "sfr_PNMFF": float, SFR according to the multi-freefall Padoan & Nordlund model
        - "R": np.ndarray, scales (in units of the jeans length for the mean density, R_tilde in Hennebelle, Brucy & Colman 2024)
        - "sfr_cumulative": np.ndarray, cumulative SFR according to the TS model
        - "mass": np.ndarray, mass of a unstable structure at a given scale (in units of the jeans mass for the mean density, M_tilde in Hennebelle, Brucy & Colman 2024)
        - "mass_spectrum_per_freefall": np.ndarray, value of the mass spectrum  divided by the freefall time at a given scale
        - "mass_spectrum_per_control_time": np.ndarray, value of the mass spectrum  divided by the control time at a given scale (\tau_ff tilde in Hennebelle, Brucy & Colman 2024)
        - "freefall_time": np.ndarray, freefall time at a given scale in units of the freefall time for the mean density (\tau_repl tilde in Hennebelle, Brucy & Colman 2024)
        - "repl_time": np.ndarray, replenishment time at a given scale of the freefall time for the mean density
        - "logrho": np.ndarray, log of the critical density at a given scale (delta in Hennebelle, Brucy & Colman 2024)
        - "pdf_rho": np.ndarray, values of the PDF at a given scale
        - "truncation_term": np.ndarray, values of the truncation term
        - "delta_global": np.ndarray, log-density values for the global PDF
        - "pdf_global": np.ndarray, values of the global PDF
    """
    G = 6.67e-8  # Gravitational constant in cgs
    pc = 3.08e18  # parsec in cm
    mp = 1.4 * 1.6e-24  # mean molecular weight in g
    eta_d = max(eta_d, 0.01)  # eta_d has to be positive for the model to work

    # the Jeans length
    jeans_length = (np.sqrt(np.pi) / 2.0) * cs / np.sqrt(G * n0 * mp) / pc

    # Normalize scale by the jeans length
    Li_norm = Li / jeans_length
    Lreso_norm = Lreso / jeans_length

    # mach number at the jeans length
    Mach_star = Mach / Li_norm**eta_v / np.sqrt(3.0)

    # T parameter for Castaing-Hopkins
    if T_CH == "fitted":
        T_CH = np.maximum(np.minimum(0.6 * (np.log10(Mach) - np.log10(4)), 0.5), 0.1)

    if not only_PN:
        # call the HC08 prediction
        results = compute_mass_spectrum(
            Mach=Mach,
            Mach_star_squared=Mach_star**2,
            eta_v=eta_v,
            eta_d=eta_d,
            Li=Li_norm,
            gamma=1,
            b=b,
            gamma2=1,
            const=0,
            nint=3,
            ycut=ycut,
            const_mag=0.0,
            gammag=0.3,
            Lreso_norm=Lreso_norm,
            pdf_kind=pdf_kind,
            T_CH=T_CH,
            S_lnrho=S_lnrho,
            scale_T_with_R=scale_T_with_R,
            **kwargs,
        )

        mass_dist = results["mass"]
        dm_v = np.diff(mass_dist, prepend=mass_dist[0])
        dens_rp_v = results["mass_spectrum_per_control_time"]
        dens_ff_v = results["mass_spectrum_per_freefall"]

        # calculate the SFR using the replenishment time
        sfr_rp = np.sum((dens_rp_v * dm_v * mass_dist))
        sfr_with_ff_time = np.sum((dens_ff_v * dm_v * mass_dist))

        cumsfr_v = np.cumsum(dens_rp_v * dm_v * mass_dist)

        results["sfr"] = sfr_rp # sfr according to the TS model
        results["sfr_with_ff_time"] = sfr_with_ff_time # sfr according to the TS model with freefall time instead of the maximum between replenishment time and freefall time
        results["jeans_length"] = jeans_length # Jeans length
        results["sfr_cumulative"] = cumsfr_v # cumulative SFR according to the TS model

    else:
        results = {}

    # PN criteria
    alpha_therm = 5 * cs**2 / (np.pi * G * mp * n0 * (Li * pc) ** 2)
    eps = 0.547 * alpha_therm

    # get PN sfr
    sfr_PN, sfr_PNMFF = sfr_padoan(
        Mach, eps, b, pdf_kind=pdf_kind, T_CH=T_CH, S_lnrho=S_lnrho
    )

    results["sfr_PN"] = sfr_PN # sfr according to the Padoan & Nordlund model
    results["sfr_PNMFF"] = sfr_PNMFF # sfr according to the multi-freefall Padoan & Nordlund model

    return results
