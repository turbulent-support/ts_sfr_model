import numpy as np
from scipy.interpolate import interp1d
from scipy.special import i0 as bessel_I0, i1 as bessel_I1, iv as bessel_I
from scipy.integrate import quad
from scipy.optimize import curve_fit


def bessel_I2(x):
    return bessel_I(2, x)


ASYMPTOTE_BESSEL = 600

#######################################################################################
#######################################################################################


def integrand_truncation_term(
    ln_rho,
    S_lnrho,
    T0,
    R,
    Li,
    eta_d,
    scale_T_with_R,
    asymptote_bessel=ASYMPTOTE_BESSEL,
):
    """
    With both the variance and T depending on the scale, assuming the ration T/S_lnrho is constant
    """

    scale_factor = 1.0 - (R / Li) ** (2.0 * eta_d)

    S_R = S_lnrho * scale_factor

    if scale_T_with_R:
        T_R = T0 * scale_factor
    else:
        T_R = T0

    if not isinstance(ln_rho, np.ndarray):
        ln_rho = np.array(ln_rho)

    if S_R == 0:
        return np.zeros_like(ln_rho)

    lambda_pdf = S_R / (2.0 * T_R**2)

    dscale_factor_dR = -(2 * eta_d) * (R / Li) ** (2.0 * eta_d - 1.0) / Li

    dS_dR = S_lnrho * dscale_factor_dR

    if scale_T_with_R:
        dT_dR = T0 * dscale_factor_dR
    else:
        dT_dR = 0

    dlamb_dR = dS_dR / (2.0 * T_R**2) - dT_dR / (T_R**3) * S_R

    u = lambda_pdf / (1.0 + T_R) - ln_rho / T_R
    mask_positive = u > 0
    x = np.zeros_like(u)
    x[mask_positive] = 2 * np.sqrt(lambda_pdf * u[mask_positive])
    mask_asymptotic = x > asymptote_bessel

    rho_dP_dR = np.zeros_like(u)

    A = np.zeros_like(u)
    B = np.zeros_like(u)
    T1 = np.zeros_like(u)
    T2 = np.zeros_like(u)

    du_dR = (
        dlamb_dR / (1.0 + T_R)
        - dT_dR * lambda_pdf / (1.0 + T_R) ** 2
        + dT_dR / T_R**2 * ln_rho
    )
    mask1 = mask_positive & np.logical_not(mask_asymptotic)

    C = (
        np.sqrt(lambda_pdf / u[mask1])
        / T_R
        * np.exp(-lambda_pdf - u[mask1] + ln_rho[mask1])
    )
    A[mask1] = C * 0.5 * (bessel_I0(x[mask1]) + bessel_I2(x[mask1]))
    B[mask1] = C * bessel_I1(x[mask1])

    # If x too big, use asymptotic form of I1
    mask2 = mask_positive & mask_asymptotic
    C = (
        np.exp(x[mask2] - lambda_pdf + ln_rho[mask2] - u[mask2])
        * np.sqrt(lambda_pdf / u[mask2])
        / T_R
        / np.sqrt(2 * np.pi * x[mask2])
    )
    A[mask2] = C * (1 - 7 / (8 * x[mask2]))
    B[mask2] = C * (1 - 3 / (8 * x[mask2]))
    T1[mask_positive] = A * (
        dlamb_dR * np.sqrt(u[mask_positive] / lambda_pdf)
        + du_dR * np.sqrt(lambda_pdf / u[mask_positive])
    )
    T2 = B * (
        -dT_dR / T_R - dlamb_dR - du_dR + dlamb_dR / 2.0 / lambda_pdf - du_dR / 2.0 / u
    )
    rho_dP_dR[mask_positive] = T1[mask_positive] + T2[mask_positive]

    if not np.all(np.isfinite(rho_dP_dR)):
        raise FloatingPointError
    if rho_dP_dR.size == 1:
        # Convert back to normal float
        rho_dP_dR = float(rho_dP_dR)
    return rho_dP_dR


def func_trunc_castainghopkins_bessel(
    ln_rho_crit,
    S_lnrho,
    T_CH,
    R,
    Li,
    eta_d,
    scale_T_with_R=True,
):
    """Calculate the truncation term (N_2) for the Castaing-Hopkins PDF

    Parameters
    ----------
    ln_rho_crit : float
        critical value of ln rho
    S_lnrho : float
        width of the PDF
    T_CH : float
        T parameter in Hopkings 2013
    R : float
        (normalized) scale
    Li : float
        (normalized) injection scale
    eta_d : float
        reduced exponent of the density power spectrum
    scale_T_with_R : bool, optional
        if True, the T_CH parameter is scaled with R, by default True

    Returns
    -------
    float
        value of the truncation term
    """

    def integrand(ln_rho):
        result = integrand_truncation_term(
            ln_rho,
            S_lnrho,
            T_CH,
            R,
            Li,
            eta_d,
            scale_T_with_R,
        )
        return result

    return quad(integrand, ln_rho_crit, 20)[0]


#######################################################################################
#######################################################################################


def pdf_CH(ln_rho, S_lnrho, T_CH, asymptote_bessel=ASYMPTOTE_BESSEL):
    """
    This calculate the Castaing-Hopkins PDF as defined in Hopkins 2013, using the bessel function


    Parameters
    ----------
    ln_rho : ndarray of float
        value(s) of ln rho for which the pdf should be calculated
    S_lnrho : float
        width of the pdf
    T_CH : float
        T parameter in Hopkings 2013
    asymptote_bessel : float, optional
        value over which the asymptotic form of the bessel function is used , by default ASYMPTOTE_BESSEL

    Returns
    -------
    np.ndarray
        values of the PDF
    """
    lambda_pdf = S_lnrho / 2.0 / (T_CH**2)
    u = lambda_pdf / (1.0 + T_CH) - ln_rho / T_CH

    if not isinstance(u, np.ndarray):
        u = np.array(u)

    mask_positive = u >= 0
    pdf = np.zeros_like(u)
    x = np.zeros_like(u)

    x[mask_positive] = 2 * np.sqrt(lambda_pdf * u[mask_positive])
    mask_asymptotic = x > asymptote_bessel

    mask1 = mask_positive & np.logical_not(mask_asymptotic)
    pdf[mask1] = (
        np.exp(-(lambda_pdf + u[mask1]))
        * np.sqrt(lambda_pdf / u[mask1])
        * bessel_I1(x[mask1])
        / T_CH
    )

    # If x too big, use asymptotic form of I1
    mask2 = mask_positive & mask_asymptotic
    pdf[mask2] = (
        np.exp(x[mask2] - lambda_pdf - u[mask2])
        * np.sqrt(lambda_pdf / u[mask2])
        / np.sqrt(2 * np.pi * x[mask2])
        / T_CH
    )

    if pdf.size == 1:
        # Convert back to normal float
        pdf = float(pdf)
    return pdf


#######################################################################################
#######################################################################################


def fit_pdf_CH(x, y, max_T_CH=1.0, yerr=None):
    """Fit the Castaing-Hopkins PDF

    Parameters
    ----------
    x : np.ndarray
        x values
    y : np.ndarray
        y values
    max_T_CH : float, optional
        maximum value for T_CH, by default 1.0
    yerr : np.ndarray, optional
        error on y, by default None

    Returns
    -------
    float
        S_lnrho
    float
        T_CH
    np.ndarray
        errors
    """

    param_opt, pcov = curve_fit(
        pdf_CH, x, y, sigma=yerr, bounds=[(0, 0), (100, max_T_CH)]
    )

    S_lnrho = param_opt[0]
    T_CH = param_opt[1]
    perr = np.sqrt(np.diag(pcov))

    return S_lnrho, T_CH, perr


def fit_pdf_CH_constrained(x, y, S_lnrho, max_T=1.0, yerr=None):
    """Fit the Castaing-Hopkins PDF with S_lnrho fixed

    Parameters
    ----------
    x : np.ndarray
        x values
    y : np.ndarray
        y values
    S_lnrho : float
        fixed S_lnrho
    max_T : float, optional
        maximum value for T_CH, by default 1.0
    yerr : np.ndarray, optional
        error on y, by default None

    Returns
    -------
    float
        T_CH
    np.ndarray
        errors
    """

    def func_for_fit_constrained(x, T_CH):
        return pdf_CH(x, S_lnrho, T_CH)

    param_opt, pcov = curve_fit(
        func_for_fit_constrained, x, y, sigma=yerr, p0=0.1, bounds=(0, max_T)
    )

    T_CH = param_opt[0]
    perr = np.sqrt(np.diag(pcov))

    return T_CH, perr


#######################################################################################
#######################################################################################
def calc_dM_dR2(mass, R, gamma, eta_v, Mach2_et, gamma2, const, nind, const_mag, gammag):
    M_R3 = mass / R**3

    ind1 = (gamma - 1.0) * nind
    ind2 = (gamma2 - 1.0) * nind

    dgammag_1 = 2.0 * gammag - 1.0

    A = (M_R3) ** ind1 + (const**nind) * ((M_R3) ** ind2)

    dA_drho = ind1 * (M_R3) ** (ind1 - 1.0) + ind2 * (const**nind) * (
        (M_R3) ** (ind2 - 1.0)
    )

    therm_mag = 1.0 / nind * (
        A ** (1.0 / nind - 1.0)
    ) * dA_drho + dgammag_1 * const_mag * ((M_R3) ** (dgammag_1 - 1.0))

    dM_dR = -3.0 * (M_R3) * (therm_mag)

    dM_dR = (
        dM_dR
        + A ** (1.0 / nind)
        + (2.0 * eta_v + 1.0) * Mach2_et * (R ** (2.0 * eta_v))
        + const_mag * ((M_R3) ** dgammag_1)
    )

    dM_dR = dM_dR / (1.0 - therm_mag / R**2)

    return dM_dR


#######################################################################################
#######################################################################################
def sign_mass2(mass, R, eta_v, gamma, Mach2_et, gamma2, const, nind, const_mag, gammag):
    gamma2 = np.double(gamma2)
    gamma = np.double(gamma)
    mass = np.double(mass)
    R = np.double(R)
    gammag = np.double(gammag)

    dgammag_1 = 2.0 * gammag - 1.0

    M_R3 = mass / R**3

    A = ((M_R3) ** (gamma - 1.0)) ** nind + (const * (M_R3) ** (gamma2 - 1.0)) ** nind

    sign = mass - R * (
        (A ** (1.0 / nind))
        + Mach2_et * (R ** (2.0 * eta_v))
        + const_mag * ((M_R3) ** dgammag_1)
    )

    return sign


#######################################################################################
#######################################################################################
def calc_mass(R, eta_v, gamma, Mach2_et, gamma2, const, nint, const_mag, gammag):
    dgammag_1 = 2.0 * gammag - 1.0

    mass_min = max(
        (
            R ** (2.0 * eta_v + 1.0) * Mach2_et,
            R ** ((4.0 - 3.0 * gamma) / (2.0 - gamma)),
            (const ** (1.0 / (2.0 - gamma2)))
            * (R ** ((4.0 - 3.0 * gamma2) / (2.0 - gamma2))),
            (const_mag ** (1.0 / (1.0 - dgammag_1)))
            * (R ** ((1.0 - 3.0 * dgammag_1) / (1.0 - dgammag_1))),
        )
    )
    mass_max = 3.0 * mass_min

    ii = 0
    while 1.0 - mass_min / mass_max > 1.0e-4:
        mass_moy = (mass_min + mass_max) / 2.0

        sign_min = sign_mass2(
            mass_min, R, eta_v, gamma, Mach2_et, gamma2, const, nint, const_mag, gammag
        )
        sign_moy = sign_mass2(
            mass_moy, R, eta_v, gamma, Mach2_et, gamma2, const, nint, const_mag, gammag
        )
        sign_max = sign_mass2(
            mass_max, R, eta_v, gamma, Mach2_et, gamma2, const, nint, const_mag, gammag
        )

        if sign_min * sign_moy < 0.0:
            mass_max = mass_moy
        else:
            if sign_moy * sign_max > 0.0:
                raise RuntimeError("Bounds are not correct")
            mass_min = mass_moy

        ii = ii + 1
        if ii > 1000:
            print("Convergence problem")
            print(R, eta_v, gamma, Mach2_et)
            print(mass_min, mass_max)
            raise RuntimeError("Convergence problem")

    mass = (mass_min + mass_max) / 2.0

    return mass


#######################################################################################
#######################################################################################
def compute_mass_spectrum(
    Mach,
    Mach_star_squared,
    eta_v,
    eta_d,
    Li,
    gamma,
    b,
    gamma2,
    const,
    nint,
    ycut,
    const_mag,
    gammag,
    Lreso_norm,
    pdf_kind="lognormal",
    T_CH=0.5,
    S_lnrho=None,
    scale_T_with_R=True,
    use_truncation_term=True,
):

    if pdf_kind not in ["lognormal", "castainghopkins"]:
        print("PDF {pdf_kind} is not supported")
        raise AttributeError("PDF {pdf_kind} is not supported")

    R_sup = Li * ycut
    R_inf = Lreso_norm

    logR_sup = np.log10(R_sup)
    logR_inf = np.log10(R_inf)

    Nbin = 200

    R_v = np.logspace(logR_inf, logR_sup, Nbin, endpoint=False)

    Mass_v = np.zeros(Nbin)
    mass_spect = np.zeros(Nbin)
    mass_spect_per_ff = np.zeros(Nbin)
    mass_spect_per_ct = np.zeros(Nbin)
    free_fall_time_v = np.ones(Nbin)
    repl_time_v = np.ones(Nbin)
    S_R_v = np.zeros(Nbin)

    PDFrho_v = np.zeros(Nbin)
    logrho_v = np.zeros(Nbin)
    truncation_term_v = np.zeros(Nbin)

    ndel = 1000
    delta_v = np.linspace(-30.0, 30, ndel)
    ddel_v = np.roll(delta_v, -1) - delta_v
    ddel_v[-1] = ddel_v[-2]

    if S_lnrho is None:
        S_lnrho = np.log(1.0 + (b**2) * (Mach) ** 2)
    sigma_2_0 = S_lnrho

    ## Calculate the function needed for the replenishment time

    # the idea is that there is a replenishment time to rejuvenate
    # a structure of size R and density n from the structure of size R' and density n'
    # Typically R' / R = n / n' => R' = R * (n/n')
    # Tau _R' = R' / sigma(R')   but sigma(R') = sigma_0 * (R'/L_0)^eta_v
    # Tau_R' = L_0^eta_v / sigma_0 * R'^(1-eta_v) = L_0^eta_v * R^(1-eta_v) * (n/n')^eta_v / sigma_0
    # now one must sum over all scales/density to get the mean Tau
    # Tau = L_0^eta_v / sigma_0 * R^(1-eta_v) * (n)^eta_v * int _-inf ^ log(n) n'*(-eta_v) * n'*PDF(n') dlog(n')
    # this is the mass weighted time
    # the function func_int4time below is int _-inf ^ log(n) n'*(-eta_v) * n'*PDF(n') dlog(n')

    if pdf_kind == "lognormal":
        norm = np.sum(
            np.exp(
                -(delta_v**2) / (2.0 * sigma_2_0)
                - sigma_2_0 / 8.0
                - delta_v / 2.0
                + 1.0 * delta_v
            )
            / np.sqrt(2.0 * np.pi)
            / np.sqrt(sigma_2_0)
            * ddel_v
        )

        integ_v = np.cumsum(
            np.exp(
                -(delta_v**2) / (2.0 * sigma_2_0)
                - sigma_2_0 / 8.0
                - delta_v / 2.0
                + eta_v * delta_v
            )
            / np.sqrt(2.0 * np.pi)
            / np.sqrt(sigma_2_0)
            * ddel_v
        )

    elif pdf_kind == "castainghopkins":
        T_R_v = np.zeros(Nbin)

        pdf_v = pdf_CH(delta_v, S_lnrho, T_CH)

        norm = np.sum(np.exp(delta_v) * ddel_v * pdf_v)
        integ_v = np.cumsum(np.exp(eta_v * delta_v) * ddel_v * pdf_v)

    integ_v = integ_v / norm
    func_int4time = interp1d(delta_v, integ_v)

    if eta_d == "fitted":
        breaking_point = 18
        if Mach < breaking_point:
            eta_d = 0.7 - np.log10(Mach) * 0.41
        else:
            eta_d = 0.27 - np.log10(Mach) * 0.07

    for i, R in enumerate(R_v):
        if R < Li * ycut and R >= Lreso_norm:

            scale_factor = 1.0 - (R / Li) ** (2.0 * eta_d)

            mass = calc_mass(
                R,
                eta_v,
                gamma,
                Mach_star_squared,
                gamma2,
                const,
                nint,
                const_mag,
                gammag,
            )
            Mass_v[i] = mass

            rho = mass / R**3

            dM_dR = calc_dM_dR2(
                mass,
                R,
                gamma,
                eta_v,
                Mach_star_squared,
                gamma2,
                const,
                nint,
                const_mag,
                gammag,
            )

            delta = np.log(mass / R**3)
            logrho_v[i] = delta
            ddelta_dR = dM_dR / mass - 3.0 / R

            if pdf_kind == "lognormal":
                sigma_2 = sigma_2_0 * scale_factor

                S_R_v[i] = sigma_2

                dsigma_dR = (
                    -(2.0 * eta_d)
                    / (2.0 * np.sqrt(sigma_2))
                    * sigma_2_0
                    / R
                    * (R / Li) ** (2.0 * eta_d)
                )

                PDFrho_v[i] = (
                    np.exp(
                        -(delta**2) / (2.0 * sigma_2) - sigma_2 / 8.0 - delta / 2.0
                    )
                    / np.sqrt(2.0 * np.pi)
                    / np.sqrt(sigma_2)
                )

                if use_truncation_term:
                    mass_spect[i] = (
                        1.0
                        / (dM_dR * mass)
                        * (
                            -ddelta_dR
                            + dsigma_dR
                            / np.sqrt(sigma_2)
                            * (max(delta, 0.0) + sigma_2 / 2.0)
                        )
                    )
                else:
                    mass_spect[i] = 1.0 / (dM_dR * mass) * (-ddelta_dR)

                mass_spect[i] = (
                    mass_spect[i]
                    * np.exp(
                        -(delta**2) / (2.0 * sigma_2) - sigma_2 / 8.0 + delta / 2.0
                    )  # PDFrho_v[i] * rho
                    / np.sqrt(2.0 * np.pi)
                    / np.sqrt(sigma_2)
                )
                if use_truncation_term:
                    truncation_term_v[i] = (
                        dsigma_dR
                        / np.sqrt(sigma_2)
                        * (max(delta, 0.0) + sigma_2 / 2.0)
                        * np.exp(
                            -(delta**2) / (2.0 * sigma_2)
                            - sigma_2 / 8.0
                            + delta / 2.0
                        )  # = PDFrho_v[i] * rho
                        / np.sqrt(2.0 * np.pi)
                        / np.sqrt(sigma_2)
                    )
                else:
                    truncation_term_v[i] = 0

            elif pdf_kind == "castainghopkins":
                S_R = S_lnrho * scale_factor
                if scale_T_with_R:
                    T_R = T_CH * scale_factor
                else:
                    T_R = T_CH

                S_R_v[i] = S_R
                T_R_v[i] = T_R
                PDFrho_v[i] = pdf_CH(delta, S_R, T_R)

                if not np.isfinite(PDFrho_v[i]):
                    raise FloatingPointError

                if use_truncation_term:
                    sum_dPdf_dR = func_trunc_castainghopkins_bessel(
                        delta,
                        S_lnrho,
                        T_CH,
                        R,
                        Li,
                        eta_d,
                        scale_T_with_R,
                    )
                    truncation_term_v[i] = sum_dPdf_dR
                else:
                    truncation_term_v[i] = 0

                mass_spect[i] = (
                    -ddelta_dR * np.exp(delta) * PDFrho_v[i] + truncation_term_v[i]
                ) / (dM_dR * mass)

            if mass_spect[i] < 0:
                mass_spect[i] = 0.0

            repl_time = (
                func_int4time(delta)
                * (Li) ** eta_v
                * (R ** (1.0 - eta_v))
                * rho ** (1.0 - eta_v)
                / Mach
            )

            control_time = max(repl_time, np.sqrt(3.0 * np.pi / 32.0) / rho**0.5)

            free_fall_time_v[i] = np.sqrt(3.0 * np.pi / 32.0) / rho**0.5
            repl_time_v[i] = repl_time
            mass_spect_per_ct[i] = mass_spect[i] / control_time
            mass_spect_per_ff[i] = mass_spect[i] / (free_fall_time_v[i])

        else:
            mass_spect[i] = 0.0

    if pdf_kind == "castainghopkins":
        pdf_global = pdf_v
    else:
        pdf_global = (
            np.exp(-((delta_v + sigma_2_0 / 2.0) ** 2) / (2.0 * sigma_2_0))
            / np.sqrt(2.0 * np.pi)
            / np.sqrt(sigma_2_0)
        )

    results = {
        "R": R_v, # scales (in units of the jeans length for the mean density, R_tilde in Hennebelle, Brucy & Colman 2024)
        "mass": Mass_v, # mass of a unstable structure at a given scale (in units of the jeans mass for the mean density, M_tilde in Hennebelle, Brucy & Colman 2024)
        "mass_spectrum": mass_spect,  # value of the mass spectrum for a given scale (or mass) - \mathcal{N} tilde in Hennebelle, Brucy & Colman 2024
        "mass_spectrum_per_freefall": mass_spect_per_ff, # value of the mass spectrum  divided by the freefall time at a given scale
        "mass_spectrum_per_control_time": mass_spect_per_ct, # value of the mass spectrum  divided by the control time at a given scale (\tau_ff tilde in Hennebelle, Brucy & Colman 2024)
        "freefall_time": free_fall_time_v, # freefall time at a given scale in units of the freefall time for the mean density (\tau_repl tilde in Hennebelle, Brucy & Colman 2024)
        "repl_time": repl_time_v, # replenishment time at a given scale of the freefall time for the mean density
        "logrho": logrho_v, # log of the critical density at a given scale (delta in Hennebelle, Brucy & Colman 2024)
        "pdf_rho": PDFrho_v, # values of the PDF at a given scale
        "truncation_term": truncation_term_v, # values of the truncation term
        "delta_global": delta_v, # log-density values for the global PDF
        "pdf_global": pdf_global, # values of the global PDF
    }

    if pdf_kind == "castainghopkins":
        results["S_R"] = S_R_v # width of the PDF at a given scale
        results["T_R"] = T_R_v # T parameter at a given scale

    return results
