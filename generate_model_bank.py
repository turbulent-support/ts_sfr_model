import numpy as np
import pandas as pd
import ts_sfr as ts
from sklearn.model_selection import ParameterGrid
from concurrent.futures import ProcessPoolExecutor
import os


def generate_theory_bank(generator, *parameters, num_processes=220):
    """Generate a bank of models using a generator function and a set of parameters

    Parameters
    ----------
    generator : function
        The function that generates the model
    parameters : dict
        The parameters to be used in the model
    num_processes : int
        Number of processes to use for parallel computation, by default 220

    Returns
    -------
    pd.DataFrame
        A dataframe with the models
    """
    param_grid = ParameterGrid(parameters)
    print(f"Doing {len(param_grid)} model computations")

    if num_processes > 1:
        with ProcessPoolExecutor(max_workers=num_processes) as executor:
            bank = list(executor.map(generator, range(len(param_grid)), param_grid))
    else:
        bank = list(map(generator, range(len(param_grid)), param_grid))

    return pd.DataFrame(bank)


def get_theorical_sfr(comp, **kwargs):
    """Get the theoretical SFR for a given set of parameters

    Parameters
    ----------
    comp : float
        Compressive fraction of the turbulent driving
    **kwargs : dict
        The parameters for the model

    Returns
    -------
    dict
        The parameters for the model with the SFR
    """
    b = 1 - (2 / 3.0) * (1 - comp)
    return ts.sfr_ts(b=b, **kwargs)


def generator_theorical_sfr(i, param_set):
    """A generator function for the theoretical SFR

    Parameters
    ----------
    i : int
        The index of the model (for progress tracking)
    param_set : dict
        The parameters for the model

    Returns
    -------
    dict
        The parameters dict updated with the SFR
    """
    if i % 1000 == 0:
        print(f"Computing model {i}")

    param_set.update(get_theorical_sfr(**param_set))
    return param_set


#######################################################################################

if __name__ == "__main__":

    # Generate the models for Paper I and II
    # Warning: can take a long time to run

    # Adjust the number of processes to the number of cores in your machine
    num_processes = os.cpu_count() - 1

    model_bank_selected = generate_theory_bank(
        generator_theorical_sfr,
        dict(
            comp=[0, 0.5, 1],
            Li=[50, 200, 333, 500],
            Mach=[5, 15, 20, 25, 50, 100],
            Lreso=[0.001],
            cs=[28834.810481],
            eta_v=[0.4],
            n0=[1, 1.5, 3, 10, 50, 100],
            eta_d=[0.1, 0.2, 0.3, 0.4, 0.5],
            ycut=[1],
            T_CH=["fitted"],
            pdf_kind=["lognormal", "castainghopkins"],
            use_truncation_term=[True],
        ),
        num_processes=num_processes,
    )

    model_bank_eta_d = generate_theory_bank(
        generator_theorical_sfr,
        dict(
            comp=[0, 0.5, 1],
            Li=[200],
            Mach=np.logspace(0, 2.5, 50),
            Lreso=[0.001],
            cs=[28834.810481],
            eta_v=[0.4],
            n0=[1.5],
            eta_d=[0.1, 0.2, 0.3, 0.4, 0.5],
            ycut=[1],
            T_CH=["fitted"],
            pdf_kind=["lognormal", "castainghopkins"],
            use_truncation_term=[True],
        ),
        num_processes=num_processes,
    )

    model_bank_scale_n0 = generate_theory_bank(
        generator_theorical_sfr,
        dict(
            comp=[0, 0.5, 1],
            Li=[50, 200, 500],
            Mach=np.logspace(0, 2.5, 50),
            Lreso=[0.001],
            cs=[28834.810481],
            eta_v=[0.4],
            n0=[1, 3, 1.5, 10, 50, 100],
            eta_d=[0.5],
            ycut=[1],
            T_CH=["fitted"],
            pdf_kind=["lognormal", "castainghopkins"],
            use_truncation_term=[True],
        ),
        num_processes=num_processes,
    )

    model_bank_truncation = generate_theory_bank(
        generator_theorical_sfr,
        dict(
            comp=[0.5],
            Li=[200],
            Mach=np.logspace(0, 2.5, 50),
            Lreso=[0.001],
            cs=[28834.810481],
            eta_v=[0.4],
            n0=[1.5],
            eta_d=[0.2, 0.5],
            ycut=[1],
            T_CH=["fitted"],
            pdf_kind=["lognormal", "castainghopkins"],
            use_truncation_term=[True, False],
        ),
        num_processes=num_processes,
    )

    model_bank_ycut = generate_theory_bank(
        generator_theorical_sfr,
        dict(
            comp=[0.5],
            Li=[50, 200, 500],
            Mach=np.logspace(0, 2.5, 50),
            Lreso=[0.001],
            cs=[28834.810481],
            eta_v=[0.4],
            n0=[1.5],
            eta_d=[0.5],
            ycut=[0.1, 0.3, 0.5, 0.7, 1],
            T_CH=["fitted"],
            pdf_kind=["lognormal", "castainghopkins"],
            use_truncation_term=[True],
        ),
        num_processes=num_processes,
    )

    model_bank_sims = generate_theory_bank(
        generator_theorical_sfr,
        dict(
            comp=[0, 0.5, 1],
            Li=[100, 333],
            Mach=np.logspace(0, 2.5, 50),
            Lreso=[2],
            cs=[28834.810481],
            eta_v=[0.4],
            n0=[1.5],
            eta_d=[0.2, 0.4],
            ycut=[0.5],
            T_CH=["fitted"],
            pdf_kind=["lognormal", "castainghopkins"],
            use_truncation_term=[True],
        ),
        num_processes=num_processes,
    )

    model_bank = pd.concat(
        [
            model_bank_selected,
            model_bank_eta_d,
            model_bank_scale_n0,
            model_bank_truncation,
            model_bank_ycut,
            model_bank_sims,
        ]
    )
    model_bank.to_pickle("model_bank.pickle")
